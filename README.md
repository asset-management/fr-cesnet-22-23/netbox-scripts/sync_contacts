# Synchronize Contacts

## Popis
Tento skript je určen pro synchronizaci kontaktů mezi objektem Device a Contact Group, který je objektu Device přidělen s použitím Custom Fieldu – pomáhá vytvářet automatizovaně Contact Assignments.

## Instalace
- **[Verze 3.4.X a níže]** Skript je nutno vložit do složky scripts, která má nejčastěji tuto cestu netbox/scripts/.
- **[Verze 3.5.X a výše]** Skript je nutné naimportovat z lokální složky. Cesta v Netboxu je Customization -> Scripts -> Add
- Viz https://docs.netbox.dev/en/stable/customization/custom-scripts/

## Výstup
![](/images/sync_contacts.gif)
![](/images/sync_contacts.png)

## Prerekvizity
1. Vytvořen objekt Custom Field s Type: Object, Object type: Tenancy->Contact Group, Content type: DCIM->Device
2. Vytvořen objekt v Contacts
3. Vytvořen objekt Contact Groups
4. Vytvořen objekt Contact Roles
5. Vytvořen objekt Device
6. Přidělena Contact Group (3. krok) v Custom Fieldu (1. krok) v objektu Device

## Postup fungování skriptu
 1. Skript projde všechny objekty Device a u každého kontroluje, zda má přidělen objekt Custom Field, který byl zvolen uživatelem jako vstupní argument.
 2. Pokud se u objektu Device najde hledaný objekt Custom Field, skript si z objektu Contact Group, který je uložen v objektu Custom Field, vybere všechny objekty Contacts a přidělí je danému objektu Device vytvořením Contact Assignmentů - o skutečnosti se vytvoří Success log.
 3. Skript také zkontroluje u daného objektu Device, zda kontakty, které jsou mu přiděleny, patří všechny do Contact Groupy, která se nachází v jeho Custom Fieldu. Pokud ne, vytvoří se o skutečnosti Warning log.

## Proměnné
**Pro správné fungování skriptu je nutné upravit globální proměnné.**

#### JOB_TIMEOUT
- Táto proměnna představuje timeout pro běh skriptu.
- Výchozí hodnota je nastavena na 300 vteřin.
```python
JOB_TIMEOUT = 300
```

## Argumenty

#### Custom Field Name
 - Povinný
 - Tento argument představuje objekt Custom Field, podle kterého se mají synchronizovat kontakty (viz Prerekvizity 1.krok).

#### Role
 - Povinný
 - Tento argument představuje objekt Contact Role, který má být použit v Contact Assignments.

