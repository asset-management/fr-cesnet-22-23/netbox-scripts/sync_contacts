from django.contrib.contenttypes.models import ContentType


from tenancy.models import ContactGroup, ContactAssignment, ContactRole
from extras.scripts import Script, ObjectVar
from extras.models import CustomField
from dcim.models import Device


__version__ = "0.2"
__author__ = "Michal Drobný"

# Timeout for the report run
JOB_TIMEOUT = 300


class CreateVirtualChassis(Script):

    job_timeout = JOB_TIMEOUT

    class Meta:
        name = "Synchronize Device Contacts"
        description = "Synchronize Device Contacts"
        field_order = ["cf_name", "role"]

    cf = ObjectVar(
        model=CustomField,
        required=True,
        label="Contact Group Custom Field",
    )
    role = ObjectVar(
        model=ContactRole,
        required=True,
        label="Role",
    )

    def run(self, data, commit):
        cf_name = data.get("cf").name
        role = data.get("role")

        for device in Device.objects.all():
            contact_group = device.custom_field_data.get(cf_name)
            if contact_group is None:
                continue
            contacts = self.__get_contacts(ContactGroup.objects.get(id=contact_group))

            for contact in contacts:
                if not ContactAssignment.objects.filter(
                    content_type=ContentType.objects.get_for_model(Device),
                    object_id=device.id,
                    contact=contact,
                    role=role
                ).exists():
                    ContactAssignment.objects.create(
                        contact=contact,
                        object=device,
                        role=role
                    )
                    self.log_success(f"Contact {contact.name} is assigned to {device.name}.")

            for contact_assignment in ContactAssignment.objects.filter(
                content_type=ContentType.objects.get_for_model(Device),
                role=role,
                object_id=device.id,
            ):
                if contact_assignment.contact not in contacts:
                    self.log_warning(f"Contact {contact_assignment.contact} is not in {contact_group}.")
    
        return
    
    def __get_contacts(self, contact_group):
        contacts = [contact for contact in contact_group.contacts.all()]

        for c_group in contact_group.children.all():
            contacts += self.__get_contacts(c_group)

        return contacts
